package com.pam.pam_proyect_v2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

import androidx.appcompat.app.AppCompatActivity;

import com.pam.pam_proyect_v2.Adapter.CategoriaAdapter;

import java.util.ArrayList;

public class Productos extends AppCompatActivity {
    private GridView gv01;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);


        gv01=(GridView) findViewById(R.id.gva1);
        ArrayList<String> nombres= new ArrayList<>();
        nombres.add("Gigantografias");
        nombres.add("Letreros luminosos");
        nombres.add("Tazas");
        nombres.add("Tazas bardot");
        nombres.add("Polos");
        nombres.add("Stickers");
        nombres.add("Afiches");
        nombres.add("Asesorias");
        nombres.add("Publicidad exterior");
        nombres.add("Viniles");


        ArrayList<Integer> numimagen = new ArrayList<>();
        numimagen.add(R.drawable.giga01);
        numimagen.add(R.drawable.letre01);
        numimagen.add(R.drawable.taza01);
        numimagen.add(R.drawable.taza02);
        numimagen.add(R.drawable.pol01);
        numimagen.add(R.drawable.sti01);
        numimagen.add(R.drawable.afi01);
        numimagen.add(R.drawable.ase01);
        numimagen.add(R.drawable.publi01);
        numimagen.add(R.drawable.vin01);

        CategoriaAdapter adapter = new CategoriaAdapter(this,nombres,numimagen);
        gv01.setAdapter(adapter);

    }

    public void MenuPrincipal(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}