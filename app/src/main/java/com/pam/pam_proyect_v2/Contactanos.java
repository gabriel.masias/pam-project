package com.pam.pam_proyect_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

import com.pam.pam_proyect_v2.Adapter.ContactanosAdapter;

import java.util.ArrayList;

public class Contactanos extends AppCompatActivity {
    private GridView gvredessociales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactanos);

        gvredessociales = (GridView) findViewById(R.id.gvaredessociales);

        ArrayList<String> appsocial = new ArrayList<>();
        appsocial.add("Publysistem E.I.R.L.");
        appsocial.add("968 079 483");
        appsocial.add("publisystem2005@hotmail.com");

        ArrayList<Integer> logo = new ArrayList<>();
        logo.add(R.drawable.fb01);
        logo.add(R.drawable.wpplogo01);
        logo.add(R.drawable.emaillogo01);

        ContactanosAdapter adapter = new ContactanosAdapter(this, appsocial, logo);
        gvredessociales.setAdapter(adapter);

    }
    public void irMenu(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}