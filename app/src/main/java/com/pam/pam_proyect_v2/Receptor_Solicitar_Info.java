package com.pam.pam_proyect_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.pam.pam_proyect_v2.Clases.SolicitarInfoFormulario;

public class Receptor_Solicitar_Info extends AppCompatActivity {
    private TextView tvreceptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receptor_solicitar_info);

        tvreceptor = (TextView) findViewById(R.id.tvreceptordatos);
        String mensaje="";
        SolicitarInfoFormulario form = (SolicitarInfoFormulario) getIntent().getExtras().getSerializable("objform");
        mensaje = form.getNombre()+"\n"+
                form.getApellidos()+"\n"+
                form.getCelular()+"\n"+
                form.getCorreo();
        tvreceptor.setText(mensaje);

    }
    public void irMenu(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void volver(View view){
        Intent intent = new Intent(this, SolicitarInformacion.class);
        startActivity(intent);
    }

}