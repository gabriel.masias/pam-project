package com.pam.pam_proyect_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.pam.pam_proyect_v2.Clases.SolicitarInfoFormulario;

import java.io.Serializable;

public class SolicitarInformacion extends AppCompatActivity {
    EditText nombre, apellido, celular, correo;
    TextView resp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_informacion);
        nombre = (EditText) findViewById(R.id.txtnombrepersona);
        apellido = (EditText) findViewById(R.id.txtapellidos);
        celular = (EditText) findViewById(R.id.txtcelular);
        correo = (EditText) findViewById(R.id.txtcorreo);
    }

    public void mostrarDatosObj(View view){
        Bundle obj = new Bundle();
        SolicitarInfoFormulario form = new SolicitarInfoFormulario();
        form.setNombre(nombre.getText().toString());
        form.setApellidos(apellido.getText().toString());
        form.setCelular(celular.getText().toString());
        form.setCorreo(correo.getText().toString());

        //lenar bundle
        obj.putSerializable("objform", (Serializable) form);
        Intent intent = new Intent(this, Receptor_Solicitar_Info.class);
        intent.putExtras(obj);
        startActivity(intent);
    }
    public void limpiardatos(View view){
        nombre.getText().clear();
        apellido.getText().clear();
        celular.getText().clear();
        correo.getText().clear();
    }
    public void irMenu(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
