package com.pam.pam_proyect_v2.Clases;

import java.io.Serializable;

public class SolicitarInfoFormulario implements Serializable {
    private String nombre;
    private String apellidos;
    private String correo;
    private String celular;

    public SolicitarInfoFormulario() {
    }

    public SolicitarInfoFormulario(String nombre, String apellidos, String correo, String celular) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correo = correo;
        this.celular = celular;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
}
