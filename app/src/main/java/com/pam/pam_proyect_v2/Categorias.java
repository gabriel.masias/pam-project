package com.pam.pam_proyect_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class Categorias extends AppCompatActivity {
    Spinner sp1;
    TextView mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);

        sp1=(Spinner) findViewById (R.id.splista);
        String [] opciones ={"Gigantografias", "Letreros luminosos", "Tazas", "Tazas bardot", "Polos","Stickers", "Afiches", "Asesorias", "Publicidad exterior", "Viniles"};

        mensaje=(TextView) findViewById(R.id.tvmostrar);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.item_categoria, opciones);

        sp1.setAdapter(adapter);
    }
    public void doMostrar( View view){
        String sel= sp1.getSelectedItem().toString();
        mensaje.setText(sel);

    }

    public void Volver(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}