package com.pam.pam_proyect_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

import com.pam.pam_proyect_v2.Adapter.IntegrantesAdapter;

import java.util.ArrayList;

public class Integrantes extends AppCompatActivity {
    private GridView gvintegrantes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integrantes);

        gvintegrantes = (GridView) findViewById(R.id.gvaintegrantes);

        ArrayList<String> nombres = new ArrayList<>();
        nombres.add("Nisa Agurto");
        nombres.add("Claudia Moscol");
        nombres.add("Sergio Berru");
        nombres.add("Martin Manzanares");
        nombres.add("Fernando Yarleque");
        nombres.add("Gabriel Masias");

        ArrayList<Integer> fotointegrante = new ArrayList<>();
        fotointegrante.add(R.drawable.integrante03);
        fotointegrante.add(R.drawable.integrante04);
        fotointegrante.add(R.drawable.integrante05);
        fotointegrante.add(R.drawable.integrante02);
        fotointegrante.add(R.drawable.integrante01);
        fotointegrante.add(R.drawable.integrante06);

        IntegrantesAdapter adapter = new IntegrantesAdapter(this, nombres, fotointegrante);
        gvintegrantes.setAdapter(adapter);

    }
    public void irMenu(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}