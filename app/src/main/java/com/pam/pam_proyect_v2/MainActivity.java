package com.pam.pam_proyect_v2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void irSobreNosotros(View view){
        Intent intent = new Intent(this, SobreNosotros.class);
        startActivity(intent);
    }
    public void irContactanos(View view){
        Intent intent = new Intent(this, Contactanos.class);
        startActivity(intent);
    }
    public void irProductos(View view){
        Intent intent = new Intent(this, Productos.class);
        startActivity(intent);
    }
    public void irCategorias(View view){
        Intent intent = new Intent(this, Categorias.class);
        startActivity(intent);
    }
    public void irIntegrantes(View view){
        Intent intent = new Intent(this, Integrantes.class);
        startActivity(intent);
    }
    public void irSolicitarInformacion(View view){
        Intent intent = new Intent(this, SolicitarInformacion.class);
        startActivity(intent);
    }
}