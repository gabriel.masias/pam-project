package com.pam.pam_proyect_v2.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pam.pam_proyect_v2.R;

import java.util.ArrayList;

public class IntegrantesAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<String> nombresintegrantes;
    private ArrayList<Integer> fotosintegrantes;

    public IntegrantesAdapter(Context context,
                              ArrayList<String> nombresintegrantes,
                              ArrayList<Integer> fotosintegrantes) {
        this.context = context;
        this.nombresintegrantes = nombresintegrantes;
        this.fotosintegrantes = fotosintegrantes;
    }

    @Override
    public int getCount() {
        return nombresintegrantes.size();
    }

    @Override
    public Object getItem(int i) {
        return nombresintegrantes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.item_integrantes, null);
        }

        ImageView imageView;
        TextView textView;

        imageView = (ImageView) view.findViewById(R.id.ivintegrante);
        textView = (TextView) view.findViewById(R.id.tvnombreintegrante);

        imageView.setImageResource(fotosintegrantes.get(i));
        textView.setText(nombresintegrantes.get(i));

        return view;
    }
}
