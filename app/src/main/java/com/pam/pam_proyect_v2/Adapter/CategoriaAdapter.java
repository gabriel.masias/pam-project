package com.pam.pam_proyect_v2.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pam.pam_proyect_v2.R;

import java.util.ArrayList;

public class CategoriaAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<String> nombres;
    private ArrayList<Integer> numimagen;


    public CategoriaAdapter(Context context,
                            ArrayList<String> nombres,
                            ArrayList<Integer> numimagen) {
        this.context = context;
        this.nombres = nombres;
        this.numimagen = numimagen;

    }

    @Override
    public int getCount() {
        return nombres.size();
    }

    @Override
    public Object getItem(int i) {
        return nombres.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView
            (int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater)
                    context.getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_productos,
                    null);
        }
        ImageView imageView =
                view.findViewById(R.id.ivintegrante);

        TextView textView = view.findViewById(R.id.tvnombreintegrante);
        textView.setText(nombres.get(i));
        imageView.setImageResource(numimagen.get(i));

        return view;
    }
}
