package com.pam.pam_proyect_v2.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pam.pam_proyect_v2.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ContactanosAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<String> appsocial;
    private ArrayList<Integer> logo;

    public ContactanosAdapter(Context context,
                              ArrayList<String> appsocial,
                              ArrayList<Integer> logo) {
        this.context = context;
        this.appsocial = appsocial;
        this.logo = logo;
    }

    @Override
    public int getCount() {
        return appsocial.size();
    }

    @Override
    public Object getItem(int i) {
        return appsocial.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView
            (int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.item_redes_sociales, null);
        }

        ImageView imageView;
        imageView = (ImageView) view.findViewById(R.id.ivlogoapp);
        TextView textView;
        textView = (TextView) view.findViewById(R.id.tvnombreapp);

        textView.setText(appsocial.get(i));
        imageView.setImageResource(logo.get(i));

        return view;
    }
}
